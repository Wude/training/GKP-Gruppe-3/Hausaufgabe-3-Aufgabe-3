#ifndef __CUBOID_H__
#define __CUBOID_H__

#include "point.h"

#include <math.h>

struct cuboid
{
  double a;
  double b;
  double c;
};

// Ein Quader, dargestellt über seine Kantenlängen.
typedef struct cuboid cuboid_t;

// Einen Quader anhand zweier Punkte ermitteln, wobei angenommen wird,
// dass der Quader an den Achsen ausgerichtet ist.
cuboid_t getCuboidBy2Points(point_t *point1, point_t *point2);

// Einen Quader auf der Konsole ausgeben.
void putCuboid(char *title, cuboid_t *cuboid);

// Das Volumen eines Quaders liefern.
double cuboidGetVolume(cuboid_t *cuboid);

// Den Flächeninhalt der Oberfläche eines Quaders liefern.
double cuboidGetSurfaceArea(cuboid_t *cuboid);

// Den Gesamtlänge der Kanten eines Quaders liefern.
double cuboidGetTotalEdgeLength(cuboid_t *cuboid);

#endif
