#include "stdafx.h"

#include "point.h"

// Einen "double"-Wert von der Konsole lesen.
double consoleGetDouble(char *title)
{
  double d;
  printf(title);
  scanf_s("%lf", &d);
  return d;
}

// Einen Punkt (3D) von der Konsole lesen.
point_t consoleGetPoint(char *title)
{
  point_t point;
  printf(title);
  point.x = consoleGetDouble("x: ");
  point.y = consoleGetDouble("y: ");
  point.z = consoleGetDouble("z: ");
  return point;
}

// Einen Punkt (3D) auf der Konsole ausgeben.
void putPoint(char *title, point_t *point)
{
  printf("%s:\n", title);
  printf("    x = %.2lf\n", point->x);
  printf("    y = %.2lf\n", point->y);
  printf("    z = %.2lf\n", point->z);
}
